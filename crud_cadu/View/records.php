<!doctype html>
<html lang="pt-br">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

  <title>Dados</title>
</head>

<body>
  <div class="container">
    <div class="row">
      <div class="col-md-12 mt-5">
        <h1 class="text-center">Produtos:</h1>
        <hr style="height: 1px;color:black;background-color:black  ;">
      </div>
    </div>
    <div class="row">
      <div class="col-md-5">
        <input type="text" class="form-control form-control-sm" placeholder="Pesquisar" aria-controls="dtBasicExample" id="myInput" onkeyup="myFunction()">
      </div>
      <div class="col-md-12">
        <table id="myTable" class="table table-striped table-bordered table-sm" style="width:100%; font-size: 13px;" cellpadding="5px" cellspacing="2px">
          <thead>
            <tr>
              <th class="th-sm"> Nome do produto </th>
              <th class="th-sm"> Marca </th>
              <th class="th-sm"> Quantidade </th>
             
            </tr>
          </thead>
          <tbody>
            <?php
            include '../crud_cadu/Model/db.php';
            $model = new Model();
            $rows = $model->fetch();
            $i = 1;
            if (!empty($rows)) {
              foreach ($rows as $row) {
            ?>
                <tr>
                  <td><?php echo $row['nome'] ?></td>
                  <td><?php echo $row['marca']; ?></td>
                  <td><?php echo $row['quantidade']; ?></td>
                  <td>
                    <a href="../crud_cadu/View/editar.php?id=<?php echo $row['id']; ?>" class="btn btn-warning">Editar</a>
                    <a href="../crud_cadu/Controller/delete.php?id=<?php echo $row['id']; ?>" class="btn btn-danger">Deletar</a>
                  </td>
                </tr>
            <?php

              }
            }
            ?>
          </tbody>
        </table><br>
        <div>
          <a href="../crud_cadu/View/cadastrar.php" class="btn btn-info">Adicionar produto</a>
          <a href="../crud_cadu/Controller/exporta_excel.php" class="btn btn-success">Baixar relatório</a>
        </div>
      </div>
    </div>
  </div>
  <!-- Optional JavaScript; choose one of the two! -->
  <script>
    function myFunction() {
      // Declare variables
      var input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("myInput");
      filter = input.value.toUpperCase();
      table = document.getElementById("myTable");
      tr = table.getElementsByTagName("tr");

      // Loop through all table rows, and hide those who don't match the search query
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
          txtValue = td.textContent || td.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }
      }
    }
  </script>
  <!-- Option 1: Bootstrap Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>


  <!-- Option 2: Separate Popper and Bootstrap JS -->
  <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
    -->
</body>

</html>