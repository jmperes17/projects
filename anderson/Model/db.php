<?php

class Model
{
    public $server = "localhost";
    public $usuario = "root";
    public $senha = "";
    public $db = "anderson";
    public $conexao;

    public function __construct()
    #Conexão com o Banco de dados
    {
        try {

            $this->conexao = new mysqli($this->server, $this->usuario, $this->senha, $this->db);
        } catch (Exception $e) {
            echo "Falha na conexão" . $e->getMessage();
        }
    }

    public function insert($nome, $marca, $quantidade)
    #Função para inserção de dados
    {

        if (
            isset($_POST['nome']) &&
            isset($_POST['marca']) &&
            isset($_POST['quantidade'])
           
        ) {

            $nome = $_POST['nome'];
            $marca = $_POST['marca'];
            $quantidade = $_POST['quantidade'];
           

            $query = " INSERT INTO produtos(nome,marca, quantidade) 
                VALUES('$nome', '$marca','$quantidade')";

            if ($sql = $this->conexao->query($query)) {
                echo "<script>alert('Dados salvos!');</script>";
                echo "<script>window.location.href='../index.php';</script>";
            } else {
                echo "<script>alert('Falha!');</script>";
                echo "<script>window.location.href='../View/cadastrar.php';</script>";
            }
        } else {
            echo "<script>alert('Preencha todos os dados!');</script>";
            echo "<script>window.location.href='../View/cadastrar.php';</script>";
        }
    }

    public function fetch()
    #Função para listar todos os campos da tabela 'records'
    {
        $data = null;

        $query = "SELECT * FROM produtos";
        if ($sql = $this->conexao->query($query)) {
            while ($row = mysqli_fetch_assoc($sql)) {
                $data[] = $row;
            }
        }
        return $data;
    }

    public function Deletar($id)
    {
        #Função para deleção de dados da tabela
        $query = "DELETE FROM produtos WHERE id = '$id'";
        if ($sql = $this->conexao->query($query)) {
            return true;
        }
    }

    public function Editar($id)
    {
        # Função para jogar os dados editar para o controller 'Update' e atualizar os dados da tabela

        $data = null;
        $query = "SELECT * FROM produtos WHERE id = '$id'";
        if ($sql = $this->conexao->query($query)) {
            while ($row = $sql->fetch_assoc()) {
                $data = $row;
            }
        }
        return $data;
    }

    public function Update($id)
    {
        # Função para realizar alteração dos dados da tabela e depois jogar pra view novamente

        if (
            isset($_POST['nome']) &&
            isset($_POST['marca']) &&
            isset($_POST['quantidade'])
            
        ) {


            $id = $_REQUEST['id'];

            $data['nome'] = $_POST['nome'];
            $data['marca'] = $_POST['marca'];
            $data['quantidade'] = $_POST['quantidade'];
          

             $queryUpdate = "UPDATE produtos SET nome = '$data[nome]', marca= '$data[marca]', quantidade = '$data[quantidade]'
              WHERE id = '$id'";

            if ($sql = $this->conexao->query($queryUpdate)) {
                echo "<script>alert('Dados alterados com sucesso!');</script>";
                echo "<script>window.location.href='../index.php';</script>";
            }
        }
    }
}
