<table id="myTable" class="table table-striped table-bordered table-sm" style="width:100%; font-size: 13px;" cellpadding="5px" cellspacing="2px">
    <thead>
        <tr scope="row">
            <th class="th-sm"> Nome do produto </th>
            <th class="th-sm"> Marca </th>
            <th class="th-sm"> Quantidade </th>

        </tr>
    </thead>
    <tbody>
        <?php
        require_once '../Model/db.php';
        $model = new Model();
        $rows = $model->fetch();
        $i = 1;
        if (!empty($rows)) {
            foreach ($rows as $row) {
        ?>
                <tr scope="row">
                    <td><?php echo $row['nome'] ?></td>
                    <td><?php echo $row['marca']; ?></td>
                    <td><?php echo $row['quantidade']; ?></td>
                </tr>
        <?php }
        }
        ?>

        <?php

        $arquivo = 'dados_produtos.xls';
        // Configurações header para forçar o download
        header("Expires: Mon, 26 Jul 2200 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-type: application/x-msexcel; charset=utf-8");
        header("Content-Disposition: attachment; filename=\"{$arquivo}\"");
        header("Content-Description: PHP Generated Data");
        exit;
        ?>