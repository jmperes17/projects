
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="estilo.css">
  <title>API CEP</title>

</head>

<body>

  <div id="div-botao">
    <form action="config.php" method="post">
      <input type="text" name="cep" id="cep" placeholder="CEP">
      <input type="submit" class="btn btn-success" name="enviar" value="Enviar">
    </form>
  </div>



  <table class="table table-striped" style="width: 80%; text-align: center; padding: 10%;">
    <thead>
      <tr>
        <th scope="col">CEP</th>
        <th scope="col">Rua</th>
        <th scope="col">Bairro</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><?= $resultado->cep ?></th>
        <td><?= $resultado->logradouro ?></td>
        <td><?= $resultado->bairro ?></td>
      </tr>

    </tbody>
  </table>

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>