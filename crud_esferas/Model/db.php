<?php

class Model
{
    public $server = "localhost";
    public $usuario = "root";
    public $senha = "";
    public $db = "crud";
    public $conexao;

    public function __construct()
    #Conexão com o Banco de dados
    {
        try {

            $this->conexao = new mysqli($this->server, $this->usuario, $this->senha, $this->db);
        } catch (Exception $e) {
            echo "Falha na conexão" . $e->getMessage();
        }
    }

    public function insert($nome, $sobrenome, $cpf, $email, $email_2, $telefone_1, $telefone_2)
    #Função para inserção de dados
    {

        if (
            isset($_POST['nome']) &&
            isset($_POST['sobrenome']) &&
            isset($_POST['cpf'])  &&
            isset($_POST['email']) &&
            isset($_POST['email_2']) &&
            isset($_POST['telefone_1']) &&
            isset($_POST['telefone_2'])
        ) {

            $nome = $_POST['nome'];
            $sobrenome = $_POST['sobrenome'];
            $cpf = $_POST['cpf'];
            $email = $_POST['email'];
            $email_2 = $_POST['email_2'];
            $telefone_1 = $_POST['telefone_1'];
            $telefone_2 = $_POST['telefone_2'];

            $query = " INSERT INTO contatos(nome,sobrenome,cpf,email,email_2,telefone_1,telefone_2) 
                VALUES('$nome', '$sobrenome','$cpf','$email','$email_2','$telefone_1','$telefone_2')";

            if ($sql = $this->conexao->query($query)) {
                echo "<script>alert('Dados salvos!');</script>";
                echo "<script>window.location.href='../index.php';</script>";
            } else {
                echo "<script>alert('Falha!');</script>";
                echo "<script>window.location.href='../View/cadastrar.php';</script>";
            }
        } else {
            echo "<script>alert('Preencha todos os dados!');</script>";
            echo "<script>window.location.href='../View/cadastrar.php';</script>";
        }
    }

    public function fetch()
    #Função para listar todos os campos da tabela 'records'
    {
        $data = null;

        $query = "SELECT * FROM contatos";
        if ($sql = $this->conexao->query($query)) {
            while ($row = mysqli_fetch_assoc($sql)) {
                $data[] = $row;
            }
        }
        return $data;
    }

    public function Deletar($id_contato)
    {
        #Função para deleção de dados da tabela
        $query = "DELETE FROM contatos WHERE id_contato = '$id_contato'";
        if ($sql = $this->conexao->query($query)) {
            return true;
        }
    }

    public function Editar($id_contato)
    {
        # Função para jogar os dados editar para o controller 'Update' e atualizar os dados da tabela

        $data = null;
        $query = "SELECT * FROM contatos WHERE id_contato = '$id_contato'";
        if ($sql = $this->conexao->query($query)) {
            while ($row = $sql->fetch_assoc()) {
                $data = $row;
            }
        }
        return $data;
    }

    public function Update($id_contato)
    {
        # Função para realizar alteração dos dados da tabela e depois jogar pra view novamente

        if (
            isset($_POST['nome']) &&
            isset($_POST['sobrenome']) &&
            isset($_POST['cpf'])  &&
            isset($_POST['email']) &&
            isset($_POST['email_2']) &&
            isset($_POST['telefone_1']) &&
            isset($_POST['telefone_2'])
        ) {


            $id_contato = $_REQUEST['id_contato'];

            $data['nome'] = $_POST['nome'];
            $data['sobrenome'] = $_POST['sobrenome'];
            $data['cpf'] = $_POST['cpf'];
            $data['email'] = $_POST['email'];
            $data['email_2'] = $_POST['email_2'];
            $data['telefone_1'] = $_POST['telefone_1'];
            $data['telefone_2'] = $_POST['telefone_2'];

             $queryUpdate = "UPDATE contatos SET nome = '$data[nome]', sobrenome= '$data[sobrenome]', cpf = '$data[cpf]',
             email = '$data[email]', email_2 = '$data[email_2]', telefone_1 = '$data[telefone_1]', telefone_2 = '$data[telefone_2]' WHERE id_contato = '$id_contato'";

            if ($sql = $this->conexao->query($queryUpdate)) {
                echo "<script>alert('Dados alterados com sucesso!');</script>";
                echo "<script>window.location.href='../index.php';</script>";
            }
        }
    }
}
