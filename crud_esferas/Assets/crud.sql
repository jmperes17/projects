-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 10-Maio-2022 às 05:12
-- Versão do servidor: 10.4.22-MariaDB
-- versão do PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `crud`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos`
--

CREATE TABLE `contatos` (
  `id_contato` int(2) NOT NULL,
  `nome` varchar(20) NOT NULL,
  `sobrenome` varchar(20) NOT NULL,
  `cpf` varchar(20) NOT NULL,
  `email` varchar(25) NOT NULL,
  `email_2` varchar(25) DEFAULT NULL,
  `telefone_1` char(11) NOT NULL,
  `telefone_2` char(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `contatos`
--

INSERT INTO `contatos` (`id_contato`, `nome`, `sobrenome`, `cpf`, `email`, `email_2`, `telefone_1`, `telefone_2`) VALUES
(1, 'Jozé ', 'Martins Peres Junior', '123', 'jmperes17@gmail.com', 'email@email.com', '21966801984', '21966801984'),
(2, 'Ana', 'Peres', '321', 'email@email.com.br', 'email@email.com.br', '21966801984', '21966801984'),
(7, 'Jão', 'Peres Martins', '053.364.193-46', 'jozejunior@yes.com.br', 'jozejunior@yes.com.br', '2196680880', '2196680114'),
(8, 'Jão', 'Peres Martins', '053.364.193-46', 'jozejunior@yes.com.br', 'jozejunior@yes.com.br', '2196680880', '2196680114'),
(9, 'Jão', 'Peres Martins', '053.364.193-46', 'jozejunior@yes.com.br', 'jozejunior@yes.com.br', '2196680880', '2196680114'),
(12, 'Jão', 'Peres Martins', '053.364.193-46', 'jozejunior@yes.com.br', 'jozejunior@yes.com.br', '2196680880', '2196680114'),
(13, 'Jão', 'Peres Martins', '053.364.193-46', 'jozejunior@yes.com.br', 'jozejunior@yes.com.br', '2196680880', '2196680114'),
(14, 'Jão', 'Peres Martins', '053.364.193-46', 'jozejunior@yes.com.br', 'jozejunior@yes.com.br', '2196680880', '2196680114');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `contatos`
--
ALTER TABLE `contatos`
  ADD PRIMARY KEY (`id_contato`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `contatos`
--
ALTER TABLE `contatos`
  MODIFY `id_contato` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
